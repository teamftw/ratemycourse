from __future__ import unicode_literals
from copy import deepcopy

from django.contrib import admin
from mezzanine.core.admin import DisplayableAdmin
from drum.links.models import Link

course_fieldsets = deepcopy(DisplayableAdmin.fieldsets)
course_fieldsets[0][1]["fields"].insert(2, "UniversityName")
course_fieldsets[0][1]["fields"].insert(3, "CourseYear")
course_fieldsets[0][1]["fields"].insert(4, "CourseCode")
course_fieldsets[0][1]["fields"].insert(5, "LecturerName")

class LinkAdmin(DisplayableAdmin):

    list_display = ("id", "title", "UniversityName", "CourseYear", "CourseCode", "LecturerName", "link", "status", "publish_date",
                    "user", "comments_count", "rating_sum")
    list_display_links = ["id"]
    list_editable = ("title", "UniversityName", "CourseYear", "CourseCode", "LecturerName", "link", "status")
    list_filter = ("status", "user__username")
    ordering = ("-publish_date",)

    fieldsets = (
        (None, {
            "fields": ("title", "UniversityName", "CourseYear", "CourseCode", "LecturerName", "link", "status", "publish_date", "user"),
        }),
    )

admin.site.unregister(Link)
admin.site.register(Link, LinkAdmin)
